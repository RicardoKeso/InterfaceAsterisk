#!/bin/bash

cp /var/log/messages /var/www/html/network/data

while [ 1 ]; do
	asterisk -rx "sip show registry" > /var/www/html/network/data/registros.dat ; sleep 15
	asterisk -rx "sip show peers" | head -n 4 > /var/www/html/network/data/peers.dat ; sleep 15
done
