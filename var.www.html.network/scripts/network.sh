#!/bin/sh

#  \n ou \t nao sao carregados em variavel

function conexoes(){
	
        saida=`netstat -anp |\
		grep "tcp\|udp" |\
		grep -v "0.0.0.0" |\
		grep -v "127.0.0.1" |\
		sed 's/::ffff://g' |\
		awk '{print $1, $4, $5, $6, $7, $8"--+"}' |\
		sed 's/ /\t/g' |\
		grep -v ":::" |\
        	grep -vE "192.168.200.4:80|TIME_WAIT"`

	dataHora=`date`
	saidaEx=`echo -n $saida | sed "s/--/ - $dataHora\n/g" | grep -v "192.168.200.4" | grep -v ":53"`

        date
        echo ""
	
        echo -n $saidaEx | sed 's/ + /\n/g' | sed 's/ +/\n/g' | sed 's/+ /\n/g' | sed 's/+//g' >> ../data/dados.dat
	
	echo -e "Prot\tSockDst\t\t\tSockSrc\t\t\tState\t\tPID/Proc\tUser"
	echo $saida | sed 's/--+ /\n/g' | sed 's/--+//g' | sed 's/ /\t/g'
}

function ping_(){

	tempMax=100;
	destino1="201.86.87.36"; #vono
	destino2="201.33.209.42"; #tellfree
	
	destino3="8.8.8.8"; 
	destino4="vpn.ip3.info"; #vpn embratel

        ping1=`ping $destino1 -c 1 | grep "bytes from" | awk '{print $7}' | sed 's/time=//'`;
        ping2=`ping $destino2 -c 1 | grep "bytes from" | awk '{print $7}' | sed 's/time=//'`;
        ping3=`ping -I eth2 $destino3 -c 1 | grep "from" | awk '{print $4, $5, $7}' | sed 's/from //' | awk '{print $1, $3}' |\
                sed 's/: time=/ / ' | sed 's/\n//g'`;
        ping4=`ping -I eth2 $destino4 -c 1 | grep "from" | awk '{print $4, $5, $7}' | sed 's/from //' | awk '{print $1, $3}' |\
                sed 's/: time=/ / ' | sed 's/\n//g'`;

        ipProvedor=`echo $ping3 | awk '{print $1}'`;

        ping1=`printf  "$ping1" "ms"`;
        ping2=`printf  "$ping2" "ms"`;

        if [ $ipProvedor == "201.30.47.130" ]; then
                ping3=$ping3" Embratel";
        elif [ $ipProvedor == "179.185.18.230" ]; then
                ping3=$ping3" GVT_FO";
        else
                ping3=$ping3" Other";
        fi


	echo $ping3 | awk '{print "\nOrigem: eth2: "$1" - "$4}';
        echo $ping3 | awk '{print "Destino: "$2"\t> Tempo: "$3" ms - DNS_Google"}';
        echo $ping4 | awk '{print "Destino: "$2"\t> Tempo: "$3" ms - VPN_iP3\n"}';

	if [ $ping1 -gt $tempMax ] || [ $ping2 -gt $tempMax ]; then
                echo "* * * ALTO ( > "$tempMax" ms) * * *";
                echo "VONO - " $ping1" ms";
                echo "TELLFREE - " $ping2" ms";
        else
                echo "* * * BAIXO ( > "$tempMax" ms)* * *";
                echo "VONO - " $ping1" ms";
                echo "TELLFREE - " $ping2" ms";
        fi
}

function shutdown_(){
        cat ../data/messages | grep shutdown | tail -n 5 | awk '{print $1, $2, $3, "2015 - "$6, $7, $8, $9, $10}'
}

function conExternas(){
        cat ../data/dados.dat | grep ESTABLISHED | wc -l
}

function registros(){
        cat ../data/registros.dat
}

function peers(){
        cat ../data/peers.dat
}

function peersAck(){
#	cat ../data/ack.dat
	echo -e "IP\t\t\tRamal/Numero";
	asterisk -rx "sip show channels" | grep ACK | \
	awk '{print $1"\t\t"$2}';
}

if [ $1 == "ping" ]; then
        ping_
elif [ $1 == "con" ]; then
        conexoes
elif [ $1 == "off" ]; then
        shutdown_
elif [ $1 == "ext" ]; then
        conExternas
elif [ $1 == "reg" ]; then
        registros
elif [ $1 == "pee" ]; then
        peers
elif [ $1 == "ack" ]; then
	peersAck	
fi


