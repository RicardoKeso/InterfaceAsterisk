#!/bin/sh

delay=30;
tempMax=100;
destino1="201.86.87.36"; #vono
destino2="201.33.209.42"; #tellfree
#destino3="8.8.8.8"; #google_1
destino3="www.google.com"; #google_1

while [ 1 ]; do

	ping1=`ping $destino1 -c 1 | grep "bytes from" | awk '{print $7}' | sed 's/time=//'`;
	ping2=`ping $destino2 -c 1 | grep "bytes from" | awk '{print $7}' | sed 's/time=//'`;
	ping3=`ping -I eth2 $destino3 -c 1 | grep "from" | awk '{print $4, $5, $7}' | sed 's/from //' | awk '{print $1, $3}' |\
 		sed 's/: time=/ / ' | sed 's/\n//g'`;

	ipProvedor=`echo $ping3 | awk '{print $1}'`;

	echo $ipProvedor $provedor

	ping1=`printf  "$ping1" "ms"`;
	#ping1=`printf "%.0f" "$ping1" "ms"`;
	ping2=`printf  "$ping2" "ms"`;
	#ping2=`printf "%.0f" "$ping2" "ms"`;

	if [ $ipProvedor == "201.30.47.130" ]; then
		ping3=$ping3" Embratel";
		echo $provedor;
	elif [ $ipProvedor == "192.168.201.3" ]; then
		ping3=$ping3" GVT";
		echo $provedor;
	else
		ping3=$ping3" Other";
		echo "";
	fi


	if [ $ping1 -gt $tempMax ] || [ $ping2 -gt $tempMax ]; then
		clear;
		#echo $ping3 | awk '{print "\nOrigem: eth2: "$1": "$4" > Destino: "$2" > Tempo: "$3" ms\n"}';
		#echo $ping3;
		echo "* * * ALTO! ( > "$tempMax" ms) * * *";
		echo "VONO - " $ping1" ms";
		echo "TELLFREE - " $ping2" ms";
	else
		clear;
		#echo $ping3 | awk '{print "\nOrigem: eth2: "$1": "$4" > Destino: "$2" > Tempo: "$3" ms\n"}';
		#echo $ping3;
		echo "* * * BAIXO! ( > "$tempMax" ms)* * *";
		echo "VONO - " $ping1" ms";
		echo "TELLFREE - " $ping2" ms";
	fi

	echo "";
	
	for ((i=0; i<$delay; i++)); do
		sleep 1;
		echo -n ".";
	done
done
