var g_iCount = 31;
var g_iCount2 = g_iCount;
var g_iCount3 = 0;
function startCountdown() {
        if ((g_iCount2 - 1) >= 0) {
                g_iCount2 = g_iCount2 - 1;
                //document.getElementById('contador').innerHTML = '' + g_iCount2;
                $( "#barProg" ).val(g_iCount3);
                setTimeout('startCountdown()', 1000);
                g_iCount3 += 1 
        } else if (g_iCount2 == 0) {
            g_iCount2 = g_iCount;
            g_iCount3 = 0;            
            $( "#barProg" ).attr("max", g_iCount - 1);
        }
};

function pingLoad(){
    $.ajax({
        url: "paginas/ping.php",
        success: function(result){
            $("#ping").html(result);
            startCountdown();
            registros();
            setTimeout('pingLoad()', g_iCount * 1000 );
        }
    });
};

function connLoad(){
    $.ajax({
        url: "paginas/conexoes.php",
        success: function(result){
            $("#conexoes").html(result);
            conExternas();
	    emLigacao();
            setTimeout('connLoad()', 2000 );
        }
    });
};

function shutdown(){
    $.ajax({
        url: "paginas/shutdown.php",
        success: function(result){
            $("#shutdown").html(result);
        }
    });
};

function conExternas(){
    $.ajax({
        url: "paginas/conexoesExternas.php",
        success: function(result){
            $("#contadores").html(result);
        }
    });
};

function registros(){
    $.ajax({
        url: "paginas/registros.php",
        success: function(result){
            $("#registros").html(result);
        }
    });
};

function emLigacao(){
    $.ajax({
        url: "paginas/emLigacao.php",
        success: function(result){
            $("#emLigacao").html(result);
        }
    });
};

$(document).ready(function(){
    $( "#barProg" ).attr("max", g_iCount - 1);
    
    shutdown();
    pingLoad();
    connLoad();
    conExternas();
    registros();

    //setInterval('pingLoad()', g_iCount * 1000 );
    //setInterval('connLoad()', 1000 );
});
