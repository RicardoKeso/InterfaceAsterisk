#!/bin/bash
#
# SCRIPT DE ALERTA, TEM POR OBJETIVO EMITIR SONS DE ALERTA
# PELO SPEAKER DA PLACA MAE, CASO DISPONIVEL
# DESENVOLVIDO POR MARICIO MAGALHAES - 2011
#
#
while :; do

ping -qc1 8.8.8.8

if [ $? -gt 0 ]; then
	/root/bin/internet_off.sh
else 
     asterisk -rx "sip show registry" | grep -c Registered >/dev/null	
	if [ $? -gt 0 ]; then
        	beep -l 100    
	fi
fi

sleep 3s

done
