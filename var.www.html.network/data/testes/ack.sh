#!/bin/bash
#
# SCRIPT RELATORIO, TEM COMO OBJETIVO DISPONIBILIZAR A INFORMACOES 
# IMPORTANTES DA CENTRAL IPBX
# DESENVOLVIDO POR MAURICIO MAGALHAES - 2011
#

clear
echo "********************************************************************"
echo "********************* Canais sendo utilizados **********************"
echo "********************************************************************"
echo
asterisk -rx "sip show channels" | grep ACK | \
awk '
BEGIN {printf "%18s %14s %10s\n","[HOST]", "[RAMAL/DID]", "[CODEC]\n" }
      {printf "=> %15s %13s %10s\n", $1, $2, $6}'

QTD=$(asterisk -rx "sip show channels" | grep -c ACK)
echo
echo CON.SIMUL: $QTD
echo
echo "*******************************************************************"
echo "************************ Canais  g729 utilizados ******************"
echo "*******************************************************************"
echo
asterisk -rx "sip show channels" | grep g729 | \
awk '
BEGIN {printf "%18s %14s %10s\n","[HOST]", "[RAMAL/DID]", "[CODEC]\n" }
      {printf "=> %15s %13s %10s\n", $1, $2, $6}'

QTD=$(asterisk -rx "sip show channels" | grep -c g729)
echo
echo LICENCAS G729 EM USO: $QTD de 11
echo
echo "*******************************************************************"
echo "*********************  PROVEDORES SIP STATUS  *********************"
echo "*******************************************************************"
echo
asterisk -rx "sip show registry" | grep Registered | \
awk '
BEGIN {printf "%15s %25s %10s\n","[PROVEDOR]", "[STATUS]", "[TEMPO]\n" }
      {printf "=> %25s %12s %4s %2s %2s %2s\n", $1, $4, $6, $7, $8, $9 }'
QTD=$(asterisk -rx "sip show registry" | grep -c Registered)
echo
echo PROVEDORES REGISTRADOS = $QTD 
asterisk -rx "sip show registry" | grep Unregistered | \
awk '{printf "=> %25s %12s %4s %2s %2s %2s\n", $1, $4, $6, $7, $8, $9 }'
QTD=$(asterisk -rx "sip show registry" | grep -c Unregistered)
echo PROVEDORES NAO REGISTRADOS = $QTD 
echo
#
echo
echo "*******************************************************************"
echo "*******************      RAMAIS ONLINE      ***********************"
echo "*******************************************************************"
echo
QTD=$(asterisk -rx "sip show peers" | grep -c OK)
echo RAMAIS ONLINE: $QTD
echo
