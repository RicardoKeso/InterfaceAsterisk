<!DOCTYPE html>

<html lang="pt-br">
        <head>
                <title>NetworkCentral</title>
                <link rel="stylesheet" type="text/css" href="styles/style.css">
		<script src="scripts/jquery-2.1.3.min.js" type="text/javascript"></script>
        </head>
        <body>
                <div id="principal">                        
                        <hr>
                        PING
                        <div id="ping">
                                ping
                        </div>
                        <progress id="barProg" max="0" value="0"></progress>                        
                        <br><br>
                        REGISTROS
                        <div id="registros">
                                registros
                        </div>
                        <div id="barraAnime">
                                <div id="progAnime"></div>
                        </div>                      
                        <br>
                        EM LIGACAO
                        <div id="emLigacao">
                        	emLigacao
			</div>
                        <hr>
			CONEXOES
                        <div id="conexoes">
                                conexoes
                        </div>
                        <hr>
                        DESLIGAMENTOS
                        <div id="shutdown">
                                shutdown
                        </div>
                        <hr>
                        CONTADORES
                        <div id="contadores">
                                contadores
                        </div>
                        <hr>
                </div>
        </body>

        <script src="scripts/script.js" type="text/javascript"></script>

</html>

